def main():
    '''  

    respuesta = [0,1,2,3]
    entrada = []
    entrada.append(respuesta)
    print(entrada)
    respuesta = [5]
    entrada.append(respuesta)
    print(entrada)
    print(entrada[1][0])
    entrada.append(88)
    print(entrada)
    r = [5,6,7]
    t = r
    t.append(784)
    print(t)
    for i in respuesta:
        if(9999 not in respuesta):
            respuesta.append(9999)
        print(i)
    print(respuesta)'''



    #MATRIZ DE ADY
    matrix = [["","empty","","","","","","empty","","",""],
              ["","","empty","","empty","","","","","",""],
              ["","","","a","","","","","","",""],
              ["","","","","","","empty","","","",""],
              ["","","","","","b","","","","",""],
              ["","","","","","","empty","","","",""],
              ["","empty","","","","","","empty","","",""],
              ["","","","","","","","","a","",""],
              ["","","","","","","","","","b",""],
              ["","","","","","","","","","","b"],
              ["","","","","","","","","","",""]]


    #DATOS
    final = [10]
    inicial = [0]
    alfabeto = ["a","b"]


    DFAtrans = subconjunto(matrix,alfabeto,final,inicial)
    
    
    print("      \n MATRIZ TRAN DFA \n     ")
    for x in DFAtrans:
        print("ESTADO = "+x.Nombre,x.DFA_estado)
        print()
        count = 0
        for i in alfabeto:
            print(" Para  Entrada = "+i+" Va al Estado:")
            print( x.destinos[count])
            print()
            count+=1


  

    DFA,finales,resto = obtener_dfa(DFAtrans)
    print("      \n DFA FINAL \n      ")
    '''
    IMPRESION PARA LISTA DFA'''
    for x in DFA:      
        count = 0
        print('ESTADO  '+Strgtipo(x.tipo)+'   '+ x.Nombre)
        for i in alfabeto:
            print('Con letra '+i+'   Va al ESTADO ',x.destinos[count])
            
            count+=1

    
    
    DFA_Mat_Min = minimizarDFA(DFA,finales,resto,alfabeto)

    print("      \n DFA MATRIZ TRAN_MINIMO \n      ")
    for x in DFA_Mat_Min:
        print("ESTADO = ",x.Nombre,x.DFA_estado)
        print("DE TIPO: "+Strgtipo(x.Tipo))
        count = 0
        for i in alfabeto:
            print(" Para  Entrada = "+i+" Va al Estado:")
            print( x.destinos[count])
            print()
            count+=1

    DFA_FINAL = obtener_dfa_min(DFA_Mat_Min)
    
    print("      \n DFA MINIMO \n      ")
    for x in DFA_FINAL:      
        count = 0
        print('ESTADO  '+Strgtipo(x.tipo)+'   '+ x.Nombre)
        for i in alfabeto:
            print('Con letra '+i+'   Va al ESTADO ',x.destinos[count])
            
            count+=1




def obtener_nroEstado(estado):
    valor = len(estado)
   
    
    return int(estado[1:valor])


class auxNombres:
       def __init__(self, Nombre, Estados):
        self.Nombre = Nombre
        self.Estados = Estados

def obtener_dfa_min(dfa_min_trans,):
    DFA_FINAL = []
    ListaNombres = []
    for t in dfa_min_trans:
        ListaNombres.append(auxNombres(t.Nombre,t.DFA_estado))
      

    for x in dfa_min_trans:
        index = 0
        aux = []
        while(len(x.destinos)>index):
            temp = x.destinos[index]
        
            for name in ListaNombres:
               
                if(temp[0] in name.Estados):
                    aux.append(name.Nombre)
                    break
                elif temp[0]==-1:
                    aux.append([-1])
                    break
            index+=1
        DFA_FINAL.append(DFA(x.Nombre,aux,x.Tipo))

    return DFA_FINAL    

        


   
    
    return DFA_FINAL


def minimizarDFA(DFA_recibido,finales,resto,alfabeto):
    PI = []
    PI.append(resto)
    PI.append(finales)
   
    Cambio = True
    #Mientras PI Cambie
    while(Cambio):
        Cambio = False
        #Para cada grupo de PI
        for x in PI:
            #Si no es unitario ralizo la iteracion
            
            if(len(x)>1):
                #POr cada letra del alfabeto
                for i in alfabeto:
                   #Para cada elemento de la lista x
                    auxiliar = []
                    for j in x:
                        #Toma el nro del nombre y lo utiliza como indice de la matrzi para recuperar los destinos del estado
                        idx = obtener_nroEstado(j)
                        destinos = DFA_recibido[idx].destinos
                        #print(x)
                        #Obtiene el destino dada la la letra del alfabeto actual
                        #print(destinos[alfabeto.index(i)])
                        #auxiliar.append(destinos[alfabeto.index(i)])
                 
                        #Devuelve a que set de PI va el Estado
                        
                        for t in PI:
                            if (destinos[alfabeto.index(i)][0] in t ):

                                
                                auxiliar.append(PI.index(t))
                            elif (destinos[alfabeto.index(i)][0] == -1):
                                    auxiliar.append([-1])

                            if(len(set(auxiliar))>1):
                              
                                temp= []
                           
                                auxiliar.pop()
                                temp = x.pop(x.index(j))
                            
                                temp2 = []
                                temp2.append(temp)
                                
                                PI.append(temp2)
                                
                                Cambio = True
   
    DFA_Matriz_Min = []
    contador=0
    for grupo in PI:
        itemIndv = grupo[0]
        posicion_en_DFA = obtener_nroEstado(itemIndv)
        destinos = DFA_recibido[posicion_en_DFA].destinos
        Nombre = "M"+str(contador)
        tipoDFA = 1

        #Asigna Tipos
       
        for estados in grupo:
            pos= obtener_nroEstado(estados)
            if DFA_recibido[pos].tipo == 2 :
                tipoDFA = 2
                break
            elif DFA_recibido[pos].tipo == 0:
                tipoDFA = 0


        
        DFA_Matriz_Min.append(matriz_dtrans_dfa(destinos,grupo,Nombre,tipoDFA))
        contador+=1
    return DFA_Matriz_Min
                      
                  

    
    

def Strgtipo(valor):
    if valor == 1 : 
        return "INTERMEDIO"
    elif valor ==0 :
        return "INICIAL"

    elif valor == 2:
        return "FINAL"


class DFA:
    def __init__(self, Nombre, destinos, tipo):
        self.Nombre = Nombre
        self.destinos = destinos
        self.tipo = tipo

#Simplifica la matriz de transicion del DFA
def obtener_dfa(dtrans):
    nombres = {}
    DFATabla = []
    finales = []
    resto = []
    for x in dtrans:
        nombres[tuple(x.DFA_estado)] = {x.Nombre}
    for x in dtrans:
        i=0
        destinosEstado = []
        while(len(x.destinos)>i):
            if(x.destinos[i] != [-1]):
                destinosEstado.append(list(nombres[tuple(x.destinos[i])]))
            else:
                destinosEstado.append([-1])
            i+=1
        DFATabla.append(DFA(x.Nombre,destinosEstado,x.Tipo))
    
        if x.Tipo == 2 :
            finales.append(x.Nombre)
        else: 
            resto.append(x.Nombre)
    
    return DFATabla,finales,resto



class matriz_dtrans_dfa:
    #PARA LA VARIABLE TIPO,  INICIAL = 0 INTERMEDIO = 1 FINAL = 2
    
    def __init__(self, destinos, DFA_estado, Nombre, Tipo):
              self.destinos = destinos
              self.DFA_estado = DFA_estado
              self.Nombre = Nombre
              self.Tipo = Tipo


#RECORDAR, el orden de los items de una fila de entradas es de acuerdo al orden del alfabeto
def subconjunto(mat_ady,alfabeto,estados_finales,inicial):
    #Lista de DEstados sin marcar
    Destados = []
    #Matriz De transicion 
    Dtrans = []
    #Set marcados
    marcados = []


    #OJO  CON LA LINEA DE ABAJO ASUMO RECIBO UNA LISTA DE INICIALES, EJEMPLO DE INICIAL [0,5], O TAMBIEN [0]
    Destados.append(mover(inicial,"empty",mat_ady))

    ''' 
    SI EL ESTADO INICIAL ES SOLO UN NUMERO ENTERO USAR LAS SGTES LINEAS

    set_inicial = [incial]
    Destados.append(mover(set_inicial,"empty",mat_ady))


    '''
    indice = 0
    while(len(Destados)>0):
        #Una cola para visitar los estados
        Actual = Destados.pop(0)

        #Marca el set actual
        marcados.append(sorted(Actual))
        
        #Set de entradas por letra de alfabeto
        entradas = []

        for a in alfabeto:
            #Aplica MOVER, para una entrada 
            M_Resultado = mover(Actual,a, mat_ady)
         
            #Aplica E-CERRADURA, utilizando la funcion mover
            EC_Resultado = sorted(mover(M_Resultado,"empty",mat_ady))

            #Check de que si llego a algun lado o no
            if(len(EC_Resultado)!=0):
                #Check de que si ya existe el set resultante
                if(Check_Destado(EC_Resultado, marcados, Destados)):
                    Destados.append(EC_Resultado)
                entradas.append(EC_Resultado,)
            else:

                entradas.append([-1])

        Nombre = "Q" + str(indice)

        Nuevo_Estado = matriz_dtrans_dfa(entradas,Actual,Nombre,Obtener_Tipo(Actual,inicial,estados_finales))
    
        Dtrans.append(Nuevo_Estado) 
        #Aumenta el indice para el nombre
        indice+=1

    return Dtrans

#Retorna 0 si es un estado inicial, 1 si es un estado intermedio y 2 si es un estado final o de acpetacion
def Obtener_Tipo(Actual,incial,finales):
    for x in incial:
        if x in Actual:
            return 0
    for x in finales:
        if x in Actual:
            return 2
    return 1

'''def Check_Destado(resultado,marcados, Destados):
    for x in Destados:

        if x == resultado :
            return False
    for x in marcados:
        if x == resultado :
            return False
    
    return True'''

def Check_Destado(resultado,marcados, Destados):

    if  resultado in Destados :
        return False
    if resultado in marcados :
        return False
    
    return True

def mover(Visitar_estados, entrada, matriz):
    estados_destinos = []
    estados_a_visitar = []
    #Para evitar que modifique Visitar_estados que se recibio
    for x in Visitar_estados:
        estados_a_visitar.append(x)

    #Para e-cerradura o  mejor dicho entrada "empty"
    if(entrada == "empty"):
        for x in estados_a_visitar:
           estados_destinos.append(x)
        

        for i in estados_a_visitar:
            j=0
            while(len(matriz[i])>j):
                
                    
                if (matriz[i][j] == entrada):
                    #Si el estado aun no se encuentra en destino añadir
                    if(j not in estados_destinos):
                        estados_destinos.append(j)
                    #Añadir a los estados a visita si este no se encuentra
                    if(j not in estados_a_visitar):
                        estados_a_visitar.append(j)
                j+=1

    #Para entradas distintas a "empty"
    else:

        for i in estados_a_visitar:
            j=0
            while(len(matriz[i])>j):
                
                    
                if (matriz[i][j] == entrada):
                    #Si el estado aun no se encuentra en destino añadir
                    if(j not in estados_destinos):
                        estados_destinos.append(j)
                j+=1
    return estados_destinos



main()