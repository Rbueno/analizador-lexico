#Ejemplo numero := numero digito | empty
# token = numero ; lado_derecho = [numero digito, empty]
import os
from operaciones_NFA_DFA import subconjunto,obtener_dfa,obtener_dfa_min,Obtener_Tipo,Strgtipo,minimizarDFA,Simulacion,DFA
import lector


class prod:
       token=""
       lado_derecho=[]

       def __init__(self, token, lado_derecho):
              self.token= token
              self.lado_derecho=lado_derecho

def leerArchivo(fname):
    file1 = open(fname, 'r')
    count = 0
    lineas= []
    while True:
        count += 1
        line = file1.readline().rstrip()
        if not line:
            break
        lineas.append(line)
    file1.close()
    return lineas



def extraerAlfabeto(param):
    tlist=[]
    for s in param:
        lett=s.split()
        tlist=tlist+lett
    return tlist
def extraerTokens(param):
    tlist=[]
    for s in param:
        lett=s.split()
        tlist=tlist+lett
    return tlist
def extraerProducciones(param):
    return param


class listaDFA:
    def __init__(self, DFA, nombre,inicial,alfabeto):
        self.DFA = DFA
        self.nombre = nombre
        self.inicial = inicial
        self.alfabeto = alfabeto
def CheckMatrizNFA(matriz):
    esNFA = False
    posVacio = matriz.columnas.index('#')
    for fila in matriz.cuerpo:

        if(fila[posVacio] != '' ):
            if(fila[posVacio] != [''] ):
                esNFA = True

    return esNFA
def aplicar_operaciones(lista_producciones):
    lista_dfa = []

    archivo = "Output/DFAlog.txt"

    if(os.path.exists(archivo)):
        os.remove(archivo)
    else:
        log = open(archivo,"x")

    logDFA = open(archivo,"a")

    for produccion in lista_producciones:

        M = produccion.get('matriz')
        MAT = M


        #Aplica la funcion subconjuto
        DFA_sub = subconjunto(MAT)





        alfabeto = []
        for simb in MAT.columnas:
            if simb != '#' :
                alfabeto.append(simb)
        if(CheckMatrizNFA(MAT)):
            logDFA.write("    Expresion:\n    ")
            logDFA.write(MAT.expr)
            logDFA.write("      \n MATRIZ TRAN DFA \n     ")

            for x in DFA_sub:
                logDFA.write("ESTADO = "+x.Nombre)
                logDFA.write("  "+''.join(x.DFA_estado))
                logDFA.write('\n ')
                count = 0


                if(len(x.destinos)>0):
                    if(type(x.destinos[0]) == list):

                        for i in alfabeto:

                            if(i != '#'):


                                logDFA.write(" \nPara  Entrada = "+i+" Va al Estado:   ")
                                logDFA.write( ','.join(x.destinos[count]))
                                logDFA.write(" \n")
                                count+=1
                    else:

                            for i in alfabeto:
                                if(i != '#'):


                                    logDFA.write(" \nPara  Entrada = "+i+" Va al Estado:   ")
                                    logDFA.write( ','.join(x.destinos[count]))
                                    logDFA.write(" \n")
                                    count+=1

        DFA_Nombrado,finales,resto = obtener_dfa(DFA_sub)
        logDFA.write("      \n DFA \n      ")


        for x in DFA_Nombrado:
            count = 0
            logDFA.write('\nESTADO  '+Strgtipo(x.tipo)+'   '+ x.Nombre)

            for i in alfabeto:
                if(i != '#'):
                    logDFA.write('\nCon letra '+i+'   Va al ESTADO ')
                    logDFA.write(''.join(x.destinos[count]))
                    count+=1



        DFA_Mat_Min = minimizarDFA(DFA_Nombrado,finales,resto,alfabeto)

        logDFA.write("      \n DFA MATRIZ TRAN_MINIMO \n      ")
        for x in DFA_Mat_Min:
            logDFA.write("ESTADO = ")
            logDFA.write(x.Nombre)
            logDFA.write(''.join(x.DFA_estado))
            logDFA.write("DE TIPO: "+Strgtipo(x.Tipo))
            count = 0
            if(len(x.destinos)>0):
                if(type(x.destinos[0]) == list):
                    for i in alfabeto:
                        if(i != '#'):

                                logDFA.write(" \nPara  Entrada = "+i+" Va al Estado:")
                                logDFA.write( ','.join(x.destinos[count]))
                                logDFA.write('\n ')
                                count+=1
                else:
                     for i in alfabeto:
                            if(i != '#'):


                                logDFA.write(" Para  Entrada = "+i+" Va al Estado:")
                                logDFA.write( ','.join(x.destinos[count]))
                                logDFA.write(" \n")
                                count+=1

        DFA_FINAL,inicial = obtener_dfa_min(DFA_Mat_Min)

        logDFA.write("      \n DFA MINIMO \n      ")
        for x in DFA_FINAL:
            count = 0
            logDFA.write('ESTADO  '+Strgtipo(x.tipo)+'   '+ x.Nombre)
            if(len(x.destinos)>0):
                if(type(x.destinos[0]) == list):
                    for i in alfabeto:

                        if(i != '#'):
                                logDFA.write('\nCon letra '+i)
                                logDFA.write('   Va al ESTADO ')
                                logDFA.write(x.destinos[count])
                                logDFA.write('\n ')
                                count+=1
                else:
                    for i in alfabeto:
                            if(i != '#'):


                                logDFA.write(" Para  Entrada = "+i+" Va al Estado:")
                                logDFA.write( ','.join(x.destinos[count]))
                                logDFA.write(" \n")
                                count+=1


        lista_dfa.append(listaDFA(DFA_FINAL,produccion["name"],inicial,alfabeto))







    return lista_dfa

def Menu(lista_DFA):
    """
    Menu principal
    """
    decision = 0
    T_Simbolo = []
    T_Simbolo.append(Tabla_Simbolos("",""))


    archivo = "Output/LogSimulacion.txt"

    if(os.path.exists(archivo)):
        os.remove(archivo)
    else:
        f= open(archivo,"x")
    while decision != 3:
        print()
        print("------------------------------------")
        print("1. Ingresar cadena")
        print("2. Tabla Simbolos")
        print("9. Salir")
        print("------------------------------------")
        print()
        decision = int(input("Ingrese la opcion: "))
        if decision == 1:
            test_input = str(input("Ingrese la cadena a evaluar: "))
            TestInput(test_input,lista_DFA, T_Simbolo)
        if decision == 2 :
            print("\n ")
            if(len(T_Simbolo)>0)  :
                for x in T_Simbolo:
                    if(x.palabra != ""):
                        print("Palabra: ",x.palabra + "  = " + x.token)
            else:
                print("TABLA VACIA")
        if decision == 9:
            print("Salir")
            exit()


class Tabla_Simbolos:
    def __init__(self, token, palabra):
        self.token = token
        self.palabra = palabra



def TestInput(test_input, listaDFA,TablaSimbolo):
    flag = False
    for x in listaDFA :

        resultado = Simulacion(x.DFA, test_input, x.inicial,x.alfabeto)
        if(resultado):
            TablaSimbolo.append(Tabla_Simbolos(x.nombre,test_input))
            flag = True
            break
    if (flag == False):
        TablaSimbolo.append(Tabla_Simbolos("No Valido",test_input))
    return TablaSimbolo
def main():
    raw_alfabeto=leerArchivo('input/alfabeto.txt')
    raw_tokens=leerArchivo('input/tokens.txt')
    raw_producciones=leerArchivo('input/producciones.txt')
    #print(raw_alfabeto)
    #print(raw_tokens)
    #print(raw_producciones)
    #extrae items en una sola lista
    alfabeto=extraerAlfabeto(raw_alfabeto)
    tokens=extraerTokens(raw_tokens)
    producciones=extraerProducciones(raw_producciones)
    #print(alfabeto)
    #print(tokens)
    #print(producciones)
    ret=lector.procesarProduccion(producciones,tokens,alfabeto)
    #print(lector.array_producciones)
    a=lector.convertirMatriz('a')
    b=lector.convertirMatriz('b')
    mat=lector.aplicarOR(a,b)
    mat=lector.aplicarCerradura(mat)
    mat=lector.aplicarConcatenacion(mat,lector.convertirMatriz('a'))
    mat=lector.aplicarConcatenacion(mat,lector.convertirMatriz('b'))
    mat=lector.aplicarConcatenacion(mat,lector.convertirMatriz('b'))
   # lector.imprimirMatriz(mat)
    lista = []
    matriz = {"name":"((a or b)* abb","matriz":mat}
    lista.append(matriz)
    dfa_LISTA = aplicar_operaciones(lista)

    Menu(dfa_LISTA)




main()
