import re

simbolos=['(',')','|','*','[',']','?','-']
countq0=-2
array_producciones=[]
archivo_salida = "Output/01_FormacionMatriz.txt"



class Matriz:
    def __init__(self,expre='default',cnt=0):
        self.cuenta=0
        self.expr=expre
        self.filas=['q'+str(cnt),'q'+str(cnt+1)]
        self.columnas=[expre,'#']
        self.cuerpo=[['q'+str(cnt+1),''],['','']]
        self.inicial=['q'+str(cnt)]
        self.final=['q'+str(cnt+1)]



def imprimirMatriz(mat):
    attrs = vars(mat)
    print('\n', file=open(archivo_salida, "a"))
    print(', '.join("%s: %s" % item for item in attrs.items()), file=open(archivo_salida, "a"))







def splitExpression(str):
    '''
    Se parten las expresiones del lado derecho segun los operadores
    El nivel es el nivel de anidamiento entre parentesis y corchetes
    Se retorna un array con los niveles en donde partir la expresion
    '''
    level=1
    pos=0
    splitBy=[]
    for s in str:
        if(s=='|' and level==1):
            splitBy.append(pos)
        elif(s=='(' or s=='['):
            level=level-1
        elif(s==')' or s==']'):
            level=level+1
        pos=pos+1
    return(splitBy)

def in_dictlist(key, value, my_dictlist):
    for this in my_dictlist:
        if str(this.get(key)) == str(value):
            return this
    return None
def borrarEnDict(fdict,keyVal,valErase):
    for x in range(0,len(fdict)-1):
        f=fdict.pop(0)
        if f.get(keyVal) != valErase:
            fdict.append(f)
    return fdict


def splitExpression(str):
    '''
    Se parten las expresiones del lado derecho segun los operadores
    El nivel es el nivel de anidamiento entre parentesis y corchetos
    Se retorna un array con los niveles en donde partir la expresion
    '''
    level=1
    pos=0
    splitBy=[]
    for s in str:
        if(s=='|' and level==1):
            splitBy.append(pos)
        elif(s=='(' or s=='['):
            level=level-1
        elif(s==')' or s==']'):
            level=level+1
        pos=pos+1
    return(splitBy)



def split(word):
    return [char for char in word]



def separadorNTerminales(lines,d):
    '''
    Separa cada no-terminal de la expresion
    '''
    s=[]
    for l in lines:
        tmp=re.split('('+str(d)+')',l)
        s=s+tmp
    return s
def separadorSimbolos(lines,nterminales):
    '''
    Separa cada simbolo de la expresion
    '''
    s=[]
    for l in lines:
        if in_dictlist("name",l,nterminales) is None:
            tmp=split(l)
        else:
            tmp=[l]
        s=s+tmp
    return s

def procesarTProduccion(produccion,iaprocesar,contrastar):
    separado=produccion.get('separado')
    if iaprocesar < len(separado):
        item=separado[iaprocesar]
    else:
        return None


def verNivel(str):
    level=1
    maxlevel=0
    pos=0
    maxlevel
    for s in str:
        if(s=='(' or s=='['):
            level=level+1
            maxlevel=max(level,maxlevel)
        elif(s==')' or s==']'):
            level=level-1
    return maxlevel

def convertirMatriz(a):
    global countq0
    countq0+=2
    return Matriz(expre=a,cnt=countq0)

def reemplazarMatrices(expresion):
    return [convertirMatriz(x) if x not in simbolos else x for x in expresion]

def elementos(a):
    if type(a) is list:
        return a
    else:
        return[a]


def sumarListas(a,b):
    return []+elementos(a)+elementos(b)



def aplicarCerradura(matriz):
    global countq0
    countq0+=2
    self=matriz
    inicialNuevo=['q'+str(countq0)]
    finalNuevo=['q'+str(countq0+1)]
    grupo = self.inicial+finalNuevo
    self.filas=self.filas+inicialNuevo+finalNuevo
    #print(type(self.expr))
    self.expr='('+self.expr+')*'
    filaNueva=[''] * len(self.columnas)
    emptyIndex=self.columnas.index('#')
    filaNueva[emptyIndex]=grupo
    self.cuerpo.append(filaNueva)
    filaNueva=[''] * len(self.columnas)
    self.cuerpo.append(filaNueva)
    for f in self.final:
        ind=self.filas.index(f)
        self.cuerpo[ind][emptyIndex]=list(set(sumarListas(self.cuerpo[ind][emptyIndex],grupo)))
    self.inicial=inicialNuevo
    self.final=finalNuevo
    return self

def index_of(val, in_list):
    try:
        return in_list.index(val)
    except ValueError:
        return -1

def crearMatrizAB(a,b):
    #imprimirMatriz(a)
    #imprimirMatriz(b)
    matrizAB=Matriz(expre='ab',cnt=0)
    matrizAB.filas=a.filas+b.filas
    columnastmp=list(set(a.columnas+b.columnas))
    matrizAB.columnas=columnastmp
    cuerpo=[]
    for x in range(0,len(a.filas)+len(b.filas)):
        cuerpo.append([])
        for y in range(0,len(columnastmp)):
            cuerpo[x].append([''])
    for t in range(0,len(columnastmp)):
        simb=columnastmp[t]
        indta=index_of(simb,a.columnas)
        if indta>=0:
            for x in range(0,len(a.filas)):
                cuerpo[x][t]=sumarListas(a.cuerpo[x][indta],cuerpo[x][t])
        indtb=index_of(simb,b.columnas)
        if indtb>=0:
            for x in range(len(a.filas),len(a.filas)+len(b.filas)):
                cuerpo[x][t]=sumarListas(b.cuerpo[x-len(a.filas)][indtb],cuerpo[x][t])




    matrizAB.cuerpo=cuerpo

    imprimirMatriz(matrizAB)
    return matrizAB


def aplicarOR(a,b):

    matrizAB=crearMatrizAB(a,b)
    global countq0
    countq0+=2
    inicialNuevo=['q'+str(countq0)]
    finalNuevo=['q'+str(countq0+1)]
    grupo = list(set(a.inicial+b.inicial))
    matrizAB.filas=matrizAB.filas+inicialNuevo+finalNuevo
    matrizAB.expr='('+a.expr+'|'+b.expr+')'
    filaNueva=[''] * len(matrizAB.columnas)
    emptyIndex=matrizAB.columnas.index('#')
    filaNueva[emptyIndex]=grupo
    matrizAB.cuerpo.append(filaNueva)
    filaNueva=[''] * len(matrizAB.columnas)
    matrizAB.cuerpo.append(filaNueva)
    for f in a.final+b.final:
        ind=matrizAB.filas.index(f)

        matrizAB.cuerpo[ind][emptyIndex]=sumarListas(matrizAB.cuerpo[ind][emptyIndex],finalNuevo)
    matrizAB.inicial=inicialNuevo
    matrizAB.final=finalNuevo
    imprimirMatriz(matrizAB)
    return matrizAB
def aplicarConcatenacion(a,b):
    matrizAB=crearMatrizAB(a,b)
    matrizAB.expr='('+a.expr+'_'+b.expr+')'
    emptyIndex=matrizAB.columnas.index('#')
    imprimirMatriz(matrizAB)
    for f in a.final:
        ind=matrizAB.filas.index(f)
        matrizAB.cuerpo[ind][emptyIndex]=list(set(sumarListas(matrizAB.cuerpo[ind][emptyIndex],b.inicial)))
    matrizAB.inicial=a.inicial
    matrizAB.final=b.final
    imprimirMatriz(matrizAB)
    return matrizAB


def operarIndividuales(expresion):
    '''
    Se procesan las expresiones segun su forma, los simbolos ya se fueron reemplazados con sus matrices
    '''
    agrupado=list(zip(expresion,expresion[1:],expresion[2:]))
    #Se analiza la expresion agrupada de a 3
    for x in range(0,len(agrupado)):
        r=agrupado[x]
        #Por cada grupo se verifica si es una operacion OR y se aplica la operacion
        #Se reemplaza y se elimina los simbolos despues del reemplazo
        if isinstance(r[0], Matriz) and isinstance(r[2], Matriz) and r[1]=='|':
            mat=aplicarOR(r[0],r[2])
            expresion[x]=mat
            expresion[x+1]=None
            expresion[x+2]=None
    expresion = [i for i in expresion if i]
    agrupado=list(zip(expresion,expresion[1:]))
    #Se analiza la expresion agrupada de a 2
    for x in range(0,len(agrupado)):
        r=agrupado[x]
        #Por cada grupo se verifica si es una concatenacion y se aplica la operacion
        #Se reemplaza y se elimina los simbolos despues del reemplazo
        if isinstance(r[0], Matriz) and isinstance(r[1], Matriz):
            mat=aplicarConcatenacion(r[0],r[1])
            expresion[x]=mat
            expresion[x+1]=None
        #Por cada grupo se verifica si es una cerradura y se aplica la operacion
        #Se reemplaza y se elimina los simbolos despues del reemplazo
        elif isinstance(r[0], Matriz) and r[1]=='*':
            mat=aplicarCerradura(r[0])
            expresion[x]=mat
            expresion[x+1]=None
    expresion = [i for i in expresion if i]
    #Se analiza la expresion agrupada de a 3
    #Se eliminan los parentesis innecesarios
    agrupado=list(zip(expresion,expresion[1:],expresion[2:]))
    for x in range(0,len(agrupado)):
        r=agrupado[x]
        if isinstance(r[1], Matriz) and  r[0]=='(' and r[2]==')' :
            expresion[x]=None
            expresion[x+2]=None
    expresion = [i for i in expresion if i]
    return expresion




def matrizVacia():
    return 'do something'
def matrizElemento(acumulado):
    return 'do something'
def matrizAcumulado(acumulado,operador):
    return 'do something'
def cerraduraUltimoResultado():
    return 'do something'
def concatenarMatriz(acumulado,item):
    return 'do something'

def lineaThompson(expresion,acumulado,operador):
    if len(expresion)>0:
        item=expresion[0]
        if item == '(':
            lineaThompson(expresion[1:],acumulado+[item],'')
        elif item == ')':
            if len(acumulado)==0:
                return matrizVacia()
            elif len(acumulado)==1:
                return matrizElemento(acumulado)
            else:
                return matrizAcumulado(acumulado,operador)
        elif item=='*':
            return cerraduraUltimoResultado()
        elif item=='|':
            return lineaThompson(expresion[1:],acumulado+[item],'|')
        else:
            if len(acumulado)==0:
                return matrizVacia()
            elif len(acumulado)==1:
                return matrizElemento(acumulado)
            else:
                return matrizAcumulado(acumulado,operador)

        return lineaThompson(expresion[1:])



def imprimirProceso(proceso):
    for i in proceso:
        if isinstance(i, Matriz):
            imprimirMatriz(i)
        else:
            print(str(i), file=open(archivo_salida, "a"))


def procesarThompson(expresion):
    '''
    Se aplican las contrucciones de Thompson para la expresion pasada como parametro
    '''
    #Se saca el nivel de anidamiento, lo cual indica cuanto se procesara la expresion
    #Se reemplaza cada simbolo con una matriz que la represente, se espera que cada expresion este en niveles del alfabeto
    #
    print('----------------------Construcciones de Thompson-----------------', file=open(archivo_salida, "a"))
    nivel=verNivel(expresion)
    proceso=expresion
    proceso=reemplazarMatrices(proceso)
    for x in range(0,nivel):
        #Se procesan las expresiones segun su forma, los simbolos ya se fueron reemplazados con sus matrices
        proceso=operarIndividuales(proceso)
        imprimirProceso(proceso)
    #print('termine')
    return proceso
    #return lineaThompson(expresion,[],'')


def agruparKey(nterminales,simbolos,alfabeto,n):
    '''
    Agrupa todas las producciones por sus identificadores y se unen todas atravez de un OR
    '''
    aux=[]
    for n in nterminales:
        aux.append(n.get('name'))
    filter=set(aux)
    tmp=list(filter)
    #Se crea un array de identificadores
    nuevo=[]
    print('----------------------Producciones agrupadas-----------------', file=open(archivo_salida, "a"))
    for t in tmp:
        item={}
        item['name']=t
        aux=[]
        soloTerminal=True
        expr=''
        sep=[]
        #Se unen todas las expresiones con un OR
        #Se actualiza la expresion y los simbolos separados
        for n in nterminales:
            if n.get('name')==t:
                aux.append(n)
                ex=n.get('expr')
                expr=expr+'('+ex+')|'
                lst=n.get('separado')
                sep=sep+['(']+lst+[')']+['|']
                for l in lst:
                    if l in tmp:
                        soloTerminal=False
        item['solo_terminal']=soloTerminal
        item['producciones']=aux
        item['palabraProcesada']=False
        item['expr']=expr[:-1]
        sep.pop()
        item['separado']=sep
        print(item, file=open(archivo_salida, "a"))
        nuevo.append(item)
    return nuevo

def nt_a_simple(nterminales,palabra):
    for n in nterminales:
        if n.get('name')==palabra:
            return n.get('separado')
    return []

def palabraProcesada(nterminales,palabra):
    for n in nterminales:
        if n.get('name')==palabra and n.get('palabraProcesada') is True:
            return True
    return False




def eliminarNTerminales(nterminales,tmplist,prods,palabraOriginal):
    '''
    Funcion recursiva que reemplaze los no terminales con sus definiciones
    '''
    if len(prods)>0:
        if prods[0] not in tmplist:
            #No es un no-terminal
            return [prods.pop(0)] + eliminarNTerminales(nterminales,tmplist,prods,palabraOriginal)
        else:
            if palabraProcesada(nterminales,prods[0]):
                #Es palabra procesada
                return nt_a_simple(nterminales,prods[0]) + eliminarNTerminales(nterminales,tmplist,prods[1:],palabraOriginal)
            else:
                #No es palabra procesada
                if prods[0] != palabraOriginal:
                    #Se procesa una la palabra nueva
                    return eliminarNTerminales(nterminales,tmplist,nt_a_simple(nterminales,prods[0]),prods[0]) + eliminarNTerminales(nterminales,tmplist,prods[1:],palabraOriginal)
                else:
                    #Retorna la palabra como esta
                    return ['#'] + eliminarNTerminales(nterminales,tmplist,prods[1:],prods[1])
                    #return [prods[0]] + eliminarNTerminales(nterminales,tmplist,prods[1:],prods[1])
    else:
        #Se acabo la recursion
        return []

def reemplazarNTerminal(nterminales):
    '''
    Se reemplazan los n-terminales con sus definiciones
    '''
    aux=[]
    for n in nterminales:
        aux.append(n.get('name'))
    filter=set(aux)
    tmp=list(filter)
    nuevo=[]
    print('---------------------------------Reemplazar N terminales--------------------------------', file=open(archivo_salida, "a"))
    for n in nterminales:
        item=n
        prods=n.get('separado')
        prods=eliminarNTerminales(nterminales,tmp,prods,n.get('name'))
        n['separado']=prods
        #Se marca la palabra como procesada
        n['palabraProcesada']=True
        item['separado']=prods
        nuevo.append(item)
        print(item, file=open(archivo_salida, "a"))
    return nuevo


def eliminarRecursionIzquierda(nterminales,n,identificador,id):
    '''
    Se procesa cada no terminal para eliminar la recursion izquierda directa
    '''
    nombre = n.get('name')
    alfa = n.get('expr').replace(nombre,'')+identificador
    alfaSeparado = n.get('separado')
    alfaSeparado.pop(0)
    alfaSeparado.append(identificador)
    item={"id":id,"name":nombre,"expr":identificador,"separado":[identificador]}
    nterminales.append(item)
    id=id+1
    item={"id":id,"name":identificador,"expr":alfa,"separado":alfaSeparado}
    nterminales.append(item)
    id=id+1
    nterminales=borrarEnDict(nterminales,'id',n.get('id'))
    return nterminales




def procesarNTerminal(nterminales,simbolos,alfabeto,n):
    '''
    Genera un array separado de cada expresion
    '''
    expr=[n.get('expr')]
    #Separa cada no-terminal de la expresion
    for nt in nterminales:
        expr=separadorNTerminales(expr,str(nt.get('name')))
    #Separa cada simbolo de la expresion
    expr=separadorSimbolos(expr,nterminales)
    expr = list(filter(None, expr))
    n["separado"]=expr
    return expr


def procesarProduccion(array,simbolos,alfabeto):
    nterminales=[]
    id=0
    #Separamos cada produccion en dos partes, segun ->
    #Se carga en una estructura item {id,nombre, expr}
    for a in array:
        id=id+1
        linea=a.split('->')
        name=linea[0].replace(" ", "")
        expr=linea[1].replace(" ", "")
        cuts=splitExpression(expr)
        #Si no hay particiones se crea el elemento con el lado derecho completo
        if cuts is []:
            item={"id":id,"name":name,"expr":expr}
            nterminales.append(item)
        else:
            l=r=0
            #El lado derecho se parte en cada nivel segun el array de particiones
            for c in cuts:
                r=c
                sel=expr[l:r]
                item={"id":id,"name":name,"expr":sel}
                nterminales.append(item)
                id=id+1
                l=c+1
            sel=expr[l:]
            item={"id":id,"name":name,"expr":sel}
            nterminales.append(item)
    idr=0
    id=id+1
    #Se procesa cada no-terminal
    print('----------------------Producciones separadas-----------------', file=open(archivo_salida, "a"))
    for n in nterminales:
        #Genera un array separado de cada expresion
        procesarNTerminal(nterminales,simbolos,alfabeto,n)
        print(n,file=open(archivo_salida, "a"))
        if(n.get('name')==n.get('separado')[0]):
            #Se procesa cada no terminal para eliminar la recursion izquierda directa
            nterminales=eliminarRecursionIzquierda(nterminales,n,'R'+str(idr),id)
    #Se ordena los no-terminales por nombre
    nterminales = sorted(nterminales, key=lambda k: k['name'])
    #Se agrupan los no-terminales por nombre en una nueva estructura
    nterminales = agruparKey(nterminales,simbolos,alfabeto,n)
    nterminales = sorted(nterminales, key=lambda k: k['solo_terminal'],reverse=True)
    print(nterminales, file=open(archivo_salida, "a"))
    nterminales = reemplazarNTerminal(nterminales)
    expreFinal=''
    separadoFinal=[]
    #Se unen las expresiones con un OR, las expresiones y los separados
    for n in nterminales:
        print('('+n.get('name')+')'+str(n.get('separado')), file=open(archivo_salida, "a"))
        ex=n.get('expr')
        expreFinal=expreFinal+'('+ex+')|'
        lst=n.get('separado')
        separadoFinal=separadoFinal+['(']+lst+[')']+['|']
        #Se procesa thompson para cada expresion
        n['matriz']=procesarThompson(n.get('separado'))
    expreFinal=expreFinal[:-1]
    separadoFinal.pop()
    global array_producciones
    array_producciones=nterminales
    print('Expresion Final:', file=open(archivo_salida, "a"))
    print(expreFinal, file=open(archivo_salida, "a"))
    #print(separadoFinal)
    #Se procesan las contrucciones de Thompson para la union de las expresiones
    return procesarThompson(separadoFinal)






def lector():
    alfabeto=['a','b','c','d','e','f','0','1','2','3','4','5','=','#']
    simbolos=['(',')','|','*','[',']','?','-']
    test=[
        'exprEqual  -> idVariable = numero | idVariable = idVariable',
        'idVariable -> letra( letra | digito | # )*',
        'numero     -> numero digito| #',
        'letra      -> a',
        'digito     -> 0'
    ]
    open(archivo_salida, 'w').close()
    #expresion='(,(,a,),),|,(,(,0,),*,),|,(,(,b,a,),)'.split(',')
    #procesarProduccion(test,simbolos,alfabeto)
    #procesarThompson(expresion)
    #tranform='N(N[a-z]?)|N(N|T|#)*|NTN'
    #split=splitExpression(tranform)
    #print(split)
    #leerExpresion(test)
    #a=convertirMatriz('a')
    #b=convertirMatriz('b')
    #mat=aplicarOR(a,b)
    #mat=aplicarCerradura(mat)
    #mat=aplicarConcatenacion(mat,convertirMatriz('a'))
    #mat=aplicarConcatenacion(mat,convertirMatriz('b'))
    #mat=aplicarConcatenacion(mat,convertirMatriz('b'))
    #imprimirMatriz(mat)
    #crearMatrizAB(a,b)

    #mat=aplicarConcatenacion(a,b)
    #mat=aplicarOR(a,b)


lector()
