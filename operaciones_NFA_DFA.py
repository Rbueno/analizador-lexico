

import os

#Recorre el DFA minimo, de acuerdo a una entrada dada
def Simulacion (DFA,palabra,inicial,alfabeto):
    
    #Archivo Log
    log = open("Output/LogSimulacion.txt","a")
    #obtiene los posibles destinos del estado inicial
    estado = DFA[obtener_nroEstado(inicial[0])]
    #Bandera que controla si se llego a un punto muerto
    bandera = True
    #Lista que almacena caracteres no validos
    noValido = ''
    
    log.write('\n')
    log.write("Simulacion con el DFA de la expresion")
    log.write(DFA[0].expresion)
    log.write("PALABRA : "+palabra)

    #Para cada caracter de la palabra
    for char in palabra:
        log.write('\n')
        log.write("ESTADO: "+estado.Nombre)
        log.write("CON "+char)
        if(char  in alfabeto):
          
            #Obtiene el estado destino de acuerdo al caracter de entrada
            EstadoDestino = estado.destinos[alfabeto.index(char)]
            
            #Chequea de que si se llego a un punto muerto.
           
            log.write("FUE AL ESTADO ")
            log.write(EstadoDestino)
          
            #Obtiene el nuevo estado al que se movera
            posEstadoDestino = obtener_nroEstado(EstadoDestino)
            estado = DFA[posEstadoDestino]
            
        else:
            noValido = char
            bandera = False
            break
    #Si no se llego a un punto muerto valida si se encuentra en un estado final
    if(bandera):
        #Si es final
        if(estado.tipo == 2):
            log.write("'\n'La entrada : "+palabra+"  es valida para el DFA")
            return True
        #Si no es final
        else:
            log.write("'\n'La entrada : "+palabra+" no es valida para el DFA")
            log.write('\n')
            return False
    else:
        if(len(noValido)>0):
            log.write("'\n'La entrada : "+palabra+" no es valida para el DFA")
            log.write("'\n'LA PALABRA CONTIENE EL SIGUIENTE CARATER/SIMBOLO  NO VALIDO ")
            log.write(noValido)
            return False

def subconjunto(mat_ady):
    #Estados iniciales y finales
    inicial = mat_ady.inicial
    estados_finales = mat_ady.final

    #Lista de DEstados sin marcar
    Destados = []
    #Matriz De transicion 
    Dtrans = []
    #Set marcados
    marcados = []


    #Aplica e-cerradura al/los inicial/es y agrega a Destados
    Destados.append(mover(inicial,'#',mat_ady))

    indice = 0
    #Mientras DEstados no esta vacio
    while(len(Destados)>0):
        #Quita un set de destados
        Actual = Destados.pop(0)

        #Marca el set 
        marcados.append(sorted(Actual))
        
        #Set de entradas por letra/simbolo del alfabeto
        entradas = []
        #Para cada letra/simbolo del alfabeto
        for a in mat_ady.columnas:
            if (a!='#'):
                #Aplica MOVER, para una entrada 
                M_Resultado = mover(Actual,a, mat_ady)
            
                #Aplica E-CERRADURA, utilizando la funcion mover
                EC_Resultado = sorted(mover(M_Resultado,'#',mat_ady))

                #Check de que si llego a algun lado o no
                if(len(EC_Resultado)!=0):
                    #Check de que si ya existe el set resultante, si no existe lo agrega a destados
                    if(Check_Destado(EC_Resultado, marcados, Destados)):
                        Destados.append(EC_Resultado)
                    entradas.append(EC_Resultado,)
                
        #Crea un nombre para el set de estados
        Nombre = "Q" + str(indice)
        #Crea el nuevo estado
        Nuevo_Estado = matriz_dtrans_dfa(entradas,Actual,Nombre,Obtener_Tipo(Actual,inicial,estados_finales),mat_ady.expr)
        #Añade el nuevo estado al DFA
        Dtrans.append(Nuevo_Estado) 
        #Aumenta el indice para el nombre
        indice+=1
    #Retorna el DFA resulante
    return Dtrans

#VISITAR_ESTADOS = CONJUNTO DE ESTADOS A VISITAR
#ENTRADA = EL SIMBOLO UTILIZADO
def mover(Visitar_estados, entrada, matriz):
    #Listas para almacenar los estados de donde se empiza a visitar y los estados visitados/destinos
    estados_destinos = []
    estados_a_visitar = []
    #Para evitar que modifique Visitar_estados que se recibio
    for x in Visitar_estados:
        estados_a_visitar.append(x)

    #Para e-cerradura o entrada "vacio"
    if(entrada == '#'):
        #Añade los estados padre
        for x in estados_a_visitar:
           estados_destinos.append(x)
        #Obtener y visitar los destinos 
        for i in estados_a_visitar:
            
            #Se obtiene las posiciones necesarias
            indexEntrada = matriz.columnas.index(entrada)
         
            indexEstado = matriz.filas.index(i)
      
            #Se obtiene el estado destinatario
            temp = matriz.cuerpo[indexEstado][indexEntrada]

                #Controla si lo que recibe es una lista

            if(type(temp) == list):
           
                for item in temp:
                            #Si el estado aun no se encuentra en destino añadir
                    if(item!=''):
                        if( item not in estados_destinos):
                            estados_destinos.append(item)
                            #Añadir a los estados a visitar si este no se encuentra
                        if( item not in estados_a_visitar):
                            estados_a_visitar.append(item)
            else:
              
                if(temp!=''):
                    #Si el estado aun no se encuentra en destino añadir
                    if( temp not in estados_destinos):
                        estados_destinos.append(temp)
                        #Añadir a los estados a visita si este no se encuentra
                    if( temp not in estados_a_visitar):
                        estados_a_visitar.append(temp)

    #Si no se recibe una entrada vacia. A diferencia de e-cerradura, solo se obtiene el estado destino sin visitar dicho estado
    else:
        #Obtener estados destinos
        for i in estados_a_visitar:
            
            indexEntrada = matriz.columnas.index(entrada)
            
            indexEstado = matriz.filas.index(i)
            
            temp = matriz.cuerpo[indexEstado][indexEntrada]

                    #Controla si lo que recibe es una lista
            
            if(type(temp) == list):
                
                for item in temp:
                    if(item!=''):
                    #Si el estado aun no se encuentra en destino añadir
                        if( item not in estados_destinos):
                            estados_destinos.append(item)          
            else:
                
                if(temp!=''):
                    #Si el estado aun no se encuentra en destino añadir
                    if( temp not in estados_destinos):
                        estados_destinos.append(temp)
    



    return estados_destinos





#Dado el nombre de un estado, obtiene su posicion en la matriz
def obtener_nroEstado(estado):
    valor = len(estado)
    
    return int(estado[1:valor])


class auxNombres:
       def __init__(self, Nombre, Estados):
        self.Nombre = Nombre
        self.Estados = Estados

#Se encarga de reemplazar set de estados {}  por su nombre correspondiente
def obtener_dfa_min(dfa_min_trans,):
    DFA_FINAL = []
    ListaNombres = []
    iniciales = []
    for t in dfa_min_trans:
        ListaNombres.append(auxNombres(t.Nombre,t.DFA_estado))
      

    for x in dfa_min_trans:
        index = 0
        aux = []
        while(len(x.destinos)>index):
            temp = x.destinos[index]
        
            for name in ListaNombres:
               
                if(temp[0] in name.Estados):
                    aux.append(name.Nombre)
                    break
            index+=1
        DFA_FINAL.append(DFA(x.Nombre,aux,x.Tipo,x.expresion))
        if(x.Tipo==0):
            iniciales.append(x.Nombre)

    return DFA_FINAL,iniciales 

        


   
    
    return DFA_FINAL


def minimizarDFA(DFA_recibido,finales,resto,alfabeto):
    #Conjunto de estados
    PI = []
    PI.append(resto)
    PI.append(finales)
   
    Cambio = True
    #Mientras PI Cambie
    while(Cambio):
        Cambio = False
        #Para cada grupo de PI
        for x in PI:
            #Si no es unitario ralizo la iteracion
            
            if(len(x)>1):
                #Por cada letra del alfabeto
                for i in alfabeto:
                    if(i!='#'):
                    #Para cada elemento de la lista x
                        auxiliar = []
                        for j in x:
                            #Toma el nro del nombre y lo utiliza como indice de la matriz para recuperar los destinos del estado
                            idx = obtener_nroEstado(j)
                            destinos = DFA_recibido[idx].destinos

                    
                            #Devuelve a que set de PI va el Estado
                            
                            for t in PI:
                                if (destinos[alfabeto.index(i)][0] in t ):

                                    
                                    auxiliar.append(PI.index(t))
                                #Si el estado actual no va a un conjunto similar al anterior, lo elminina del grupo y lo agrega a PI
                                if(len(set(auxiliar))>1):
                                
                                    temp= []
                            
                                    auxiliar.pop()
                                    temp = x.pop(x.index(j))
                                
                                    temp2 = []
                                    temp2.append(temp)
                                    
                                    PI.append(temp2)
                                    #PI cambio
                                    Cambio = True
    #Crea el nuevo DFA de aacuerdo a lo que PI contiene
    DFA_Matriz_Min = []
    contador=0
    for grupo in PI:
        itemIndv = grupo[0]
        posicion_en_DFA = obtener_nroEstado(itemIndv)
        destinos = DFA_recibido[posicion_en_DFA].destinos
        expresion = DFA_recibido[posicion_en_DFA].expresion
        Nombre = "M"+str(contador)
        tipoDFA = 1

        #Asigna Tipos
       
        for estados in grupo:
            pos= obtener_nroEstado(estados)
            if DFA_recibido[pos].tipo == 2 :
                tipoDFA = 2
                break
            elif DFA_recibido[pos].tipo == 0:
                tipoDFA = 0


        
        DFA_Matriz_Min.append(matriz_dtrans_dfa(destinos,grupo,Nombre,tipoDFA,expresion))
        contador+=1

    #Retorna el DFA minimo
    return DFA_Matriz_Min
                      
                  

    
    
#Retorna un string correspondiente al tipo de Estado
def Strgtipo(valor):
    if valor == 1 : 
        return "INTERMEDIO"
    elif valor ==0 :
        return "INICIAL"

    elif valor == 2:
        return "FINAL"


class DFA:
    def __init__(self, Nombre, destinos, tipo,expresion):
        self.Nombre = Nombre
        self.destinos = destinos
        self.tipo = tipo
        self.expresion = expresion

#Simplifica la matriz de transicion del DFA
def obtener_dfa(dtrans):
    nombres = {}
    DFATabla = []
    finales = []
    resto = []
    for x in dtrans:
        nombres[tuple(x.DFA_estado)] = {x.Nombre}
    for x in dtrans:
        i=0
        destinosEstado = []
        while(len(x.destinos)>i):
            destinosEstado.append(list(nombres[tuple(x.destinos[i])]))
            i+=1
        DFATabla.append(DFA(x.Nombre,destinosEstado,x.Tipo,x.expresion))
    
        if x.Tipo == 2 :
            finales.append(x.Nombre)
        else: 
            resto.append(x.Nombre)
    
    return DFATabla,finales,resto



class matriz_dtrans_dfa:
    #PARA LA VARIABLE TIPO,  INICIAL = 0 INTERMEDIO = 1 FINAL = 2
    
    def __init__(self, destinos, DFA_estado, Nombre, Tipo,expresion):
              self.destinos = destinos
              self.DFA_estado = DFA_estado
              self.Nombre = Nombre
              self.Tipo = Tipo
              self.expresion = expresion


#Retorna 0 si es un estado inicial, 1 si es un estado intermedio y 2 si es un estado final o de acpetacion
def Obtener_Tipo(Actual,incial,finales):
    for x in incial:
        if x in Actual:
            return 0
    for x in finales:
        if x in Actual:
            return 2
    return 1

#Chequea si un set de estados ya se marco o si se encuentra dentro de DEstados. Si no es asi, entonces se considera como un estado nuevo
def Check_Destado(resultado,marcados, Destados):

    if  resultado in Destados :
        return False
    if resultado in marcados :
        return False
    
    return True
